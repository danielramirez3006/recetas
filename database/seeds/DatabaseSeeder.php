<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriasSeeder::class); // Llama al seeder que creamos (CategoriasSeeder)
        $this->call(UsuarioSeeder::class);
        // $this->call(UserSeeder::class);
    }
}
