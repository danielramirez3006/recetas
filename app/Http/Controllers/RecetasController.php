<?php

namespace App\Http\Controllers;

use App\CategoriaReceta;
use App\Recetas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image; 


class RecetasController extends Controller
{

    /**
     *    Para que solo se puede acceder a las recetas si se esta logeado
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'show']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* SE puede mostrar las recetas de un usuario con cualquiera de las dos formas siguientes 
           Auth::user()->recetas->dd();
           auth()->user()->recetas->dd();
        */

        // $recetas = auth()->user()->recetas;

        $usuario = auth()->user()->id;
        // Recetas con paginación
        $recetas = Recetas::where('user_id', $usuario)->paginate(10);

        // Redireccionar
        return view('recetas.index')->with('recetas',$recetas);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // DB::table('categoria_receta')->get()->dd();
        // Obtener las categorias sin modelo
        // $categorias = DB::table('categoria_recetas')->get()->pluck('nombre', 'id');      

        // Obtener las categorias con modelo
        $categorias = CategoriaReceta::all(['id','nombre']);
        
        return  view('recetas.create')->with('categorias', $categorias);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //  dd( $request['imagen']->store('upload-recetas', 'public') );

        // Almacenar datos en la BD
        // Creamos una variable que almacene el request
        $data = request()->validate([
            'titulo' => 'required|min:6',
            'categoria' => 'required',
            'preparacion' => 'required',
            'ingredientes' => 'required',
            'imagen' => 'required|image'
        ]);

        $ruta_imagen = $request['imagen']->store('upload-recetas', 'public');
        // resize de la imagen utilizando intervention image (previamente importado con facades)
        $img = Image::make (public_path("storage/{$ruta_imagen}"))->fit(1000, 550);
        $img -> save(); 
        
        // Mediante Facades guardamos los datos en la BD
        // La base de datos debe estar ya con los respectivos cambios
        // Se importa la clase use Illuminate\Support\Facades\DB; para que se pueda hacer uso del facade 
        // Insertar datos sin modelo
        /*
            DB::table('recetas')->insert([
                'titulo' => $data['titulo'],
                'preparacion' => $data['preparacion'],
                'ingredientes' => $data['ingredientes'],
                'imagen' => $ruta_imagen,
                'user_id' => Auth::user()->id, // para que detecte el id del usuario autenticado
                'categoria_id' => $data['categoria']
            ]);
        */    

        // Insertar datos con modelo
        auth()->user()->recetas()->create([  
            'titulo' => $data['titulo'],
            'preparacion' => $data['preparacion'],
            'ingredientes' => $data['ingredientes'],
            'imagen' => $ruta_imagen,
            'categoria_id' => $data['categoria']
        ]);

        // REDIRECCIONAR
        // Mediante el método redirect le decimos que nos mande al accion index de el controlador recetas
        return redirect()->action('RecetasController@index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recetas  $recetas
     * @return \Illuminate\Http\Response
     */
    public function show(Recetas $recetas)
    {
        return view('recetas.show', compact('recetas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recetas  $recetas
     * @return \Illuminate\Http\Response
     */
    public function edit(Recetas $recetas)
    {
        // Revisar el policy
        $this->authorize('view', $recetas);

        // Obtener las categorias con modelo
        $categorias = CategoriaReceta::all(['id','nombre']);
        
       return  view('recetas.edit', compact('categorias', 'recetas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recetas  $recetas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recetas $recetas)
    {
        // Revisar que se cumpla el policy
        $this->authorize('update', $recetas);

        //validacion 
        $data = request()->validate([
            'titulo' => 'required|min:6',
            'categoria' => 'required',
            'preparacion' => 'required',
            'ingredientes' => 'required',
        ]);
            
        // Asignar los valores
        $recetas->titulo = $data['titulo'];
        $recetas->categoria_id = $data['categoria'];
        $recetas->preparacion = $data['preparacion'];
        $recetas->ingredientes = $data['ingredientes'];

        // si el usuario sube una imagen 
        if(request('imagen')) {
            // Obtener la ruta de la imagen
            $ruta_imagen = $request['imagen']->store('upload-recetas', 'public');

            // Resize de la imagen utilizando intervention image (previamente importado con facades)
            $img = Image::make (public_path("storage/{$ruta_imagen}"))->fit(1000, 550);

            // Asignar al modelo
            $recetas->imagen = $ruta_imagen;
        }

        $recetas->save();

        // Redireccionar
        return redirect()->action('RecetasController@index');

    }        

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recetas  $recetas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recetas $recetas)
    {

        return "Eliminando";

        // Revisar que se cumpla el policy
        $this->authorize('delete', $recetas);

        // Eliminar receta
        $recetas->delete();

        // Redireccionar
        return redirect()->action('RecetasController@index');
    }
}
