<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Closure
Route::get('/', function () {
    return view('welcome');
});

// Rutas para las recetas
Route::get('/recetas', 'RecetasController@index')->name('recetas.index');
Route::get('/recetas/create', 'RecetasController@create')->name('recetas.create');
Route::post('/recetas', 'RecetasController@store')->name('recetas.store');
Route::get('/recetas/{recetas}', 'RecetasController@show')->name('recetas.show');
Route::get('/recetas/{recetas}/edit', 'RecetasController@edit')->name('recetas.edit');
Route::put('/recetas/{recetas}', 'RecetasController@update')->name('recetas.update');
Route::delete('/recetas/{recetas}', 'RecetasController@destroy')->name('recetas.destroy');

// Simplificar todas las rutas anteriores de recetas, engloba a todos los metodos
// Route::resource('recetas', 'RecetasController');

// Rutas para los perfiles
Route::get('/perfiles/{perfil}', 'PerfilController@show')->name('perfiles.show');
Route::get('/perfiles/{perfil}/edit', 'PerfilController@edit')->name('perfiles.edit');
Route::put('/perfiles/{perfil}', 'PerfilController@update')->name('perfiles.update');

Auth::routes();

// Controller
// Route::get('/home', 'HomeController@index')->name('home');
